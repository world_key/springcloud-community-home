import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/floor/'

// 获取楼
export function getFloor (query) {
  return request({
    url: baseUserUrl + 'getFloor',
    method: 'get',
    params: query
  })
}

// 根据小区获取获取楼栋、单元、房间（代表）树信息
export function fetchTree (query) {
  return request({
    url: baseUserUrl + 'FloorUnitRoomTreeDto',
    method: 'get',
    params: query
  })
}
