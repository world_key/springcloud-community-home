import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/voting/'

// 查询是否投票
export function findIsvote (query) {
  return request({
    url: baseUserUrl + 'findIsvote',
    method: 'get',
    params: query
  })
}

// 投票信息
export function getVotingList (query) {
  return request({
    url: baseUserUrl + 'getVotingList',
    method: 'get',
    params: query
  })
}

// 进行投票
export function castVote (obj) {
  return request({
    url: baseUserUrl + 'castVote',
    method: 'post',
    params: obj
  })
}
