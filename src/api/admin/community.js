import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/community/'

// 获取小区列表
export function getCommunityList () {
  return request({
    url: baseUserUrl + 'getCommunityList',
    method: 'get'
  })
}

export function findCommunity (userId) {
  return request({
    url: baseUserUrl + 'findCommunity/' + userId,
    method: 'get'
  })
}
