import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/user/'

export function fetchList (query) {
  return request({
    url: baseUserUrl + 'userList',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: baseUserUrl,
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: baseUserUrl + id,
    method: 'get'
  })
}

export function delObj (id) {
  return request({
    url: baseUserUrl + id,
    method: 'delete'
  })
}

export function putObj (obj) {
  return request({
    url: baseUserUrl,
    method: 'put',
    data: obj
  })
}

export function updateObjInfo (obj) {
  return request({
    url: baseUserUrl + 'updateInfo',
    method: 'put',
    data: obj
  })
}

export function updatePassword (obj) {
  return request({
    url: baseUserUrl + 'updatePassword',
    method: 'put',
    data: obj
  })
}

export function updateAvatar (obj) {
  return request({
    url: baseUserUrl + 'updateAvatar',
    method: 'put',
    data: obj
  })
}

export function delAllObj (obj) {
  return request({
    url: baseUserUrl + 'deleteAll',
    method: 'post',
    data: obj
  })
}

export function checkExist (username, tenantCode) {
  return request({
    url: baseUserUrl + 'checkExist/' + username,
    method: 'get',
    params: { tenantCode, identityType: 1 }
  })
}

export function exists (identifier) {
  return request({
    url: baseUserUrl + 'exists/' + identifier,
    method: 'get'
  })
}

// 获取短信验证码
export function getPhoneCode (obj) {
  return request({
    url: baseUserUrl + 'getPhoneCode',
    method: 'post',
    params: obj
  })
}

// 修改手机号
export function bindPhone (obj) {
  return request({
    url: baseUserUrl + 'bindPhone',
    method: 'put',
    data: obj
  })
}

// 根据手机号查询认证信息
export function findAuthInfo (query) {
  return request({
    url: baseUserUrl + 'findAuthInfo',
    method: 'get',
    params: query
  })
}

// 新增认证信息
export function sjAuth (obj) {
  return request({
    url: baseUserUrl + 'sjAuth',
    method: 'put',
    data: obj
  })
}

// 根据userId获取用户信息，包括头像与照片墙
export function getUserData (id) {
  return request({
    url: baseUserUrl + 'getUserData/' + id,
    method: 'get'
  })
}

// 添加家庭成员账户
export function addFamilyMembers (obj) {
  return request({
    url: baseUserUrl + 'addFamilyMembers',
    method: 'put',
    data: obj
  })
}

// 根据parentUserId查询
export function findByPuserId (obj) {
  return request({
    url: baseUserUrl + 'findByPuserId',
    method: 'post',
    params: obj
  })
}

// 删除家庭账户
export function deleteFamily (obj) {
  return request({
    url: baseUserUrl + 'deleteFamily',
    method: 'post',
    data: obj
  })
}
