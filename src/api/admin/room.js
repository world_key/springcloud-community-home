import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/room/'

// 获取室与投票区域
export function getRoomList (query) {
  return request({
    url: baseUserUrl + 'getRoomList',
    method: 'get',
    params: query
  })
}

// 获取室
export function getRoom (query) {
  return request({
    url: baseUserUrl + 'getRoom',
    method: 'get',
    params: query
  })
}

// 通过userId 与 小区id  查询当前小区是否是代表
export function getPartyById (query) {
  return request({
    url: baseUserUrl + 'getPartyById',
    method: 'get',
    params: query
  })
}
