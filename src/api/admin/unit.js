import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/unit/'

// 获取单元
export function getUnit (query) {
  return request({
    url: baseUserUrl + 'getUnit',
    method: 'get',
    params: query
  })
}
