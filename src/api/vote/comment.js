import request from '@/router/axios'

const baseUserUrl = '/api/vote/v1/comment/'

export function getCommentList (articleId) {
  return request({
    url: baseUserUrl + 'commentList/' + articleId,
    method: 'get'
  })
}

// 发布评论
export function addComment (obj) {
  return request({
    url: baseUserUrl + 'addComment',
    method: 'post',
    data: obj
  })
}
