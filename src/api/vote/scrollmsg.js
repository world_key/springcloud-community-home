import request from '@/router/axios'

const baseUserUrl = '/api/vote/v1/scrollMsg/'

export function fetchList (query) {
  return request({
    url: baseUserUrl + 'scrollMsgList',
    method: 'get',
    params: query
  })
}

export function getObj (id) {
  return request({
    url: baseUserUrl + id,
    method: 'get'
  })
}

// 新增发布信息
export function insertIssue (obj) {
  return request({
    url: baseUserUrl + 'insertIssue',
    method: 'put',
    data: obj
  })
}
