import request from '@/router/axios'

const baseUserUrl = '/api/vote/v1/decision/'

// 通过小区、userid 与 消息id 查询是否可以发表意见
export function findIdea (query) {
  return request({
    url: baseUserUrl + 'findIdea',
    method: 'get',
    params: query
  })
}

// 新增决策状态
export function addDecision (obj) {
  return request({
    url: baseUserUrl + 'addDecision',
    method: 'post',
    data: obj
  })
}

export function getDecisionState (query) {
  return request({
    url: baseUserUrl + 'getDecisionState',
    method: 'get',
    params: query
  })
}
