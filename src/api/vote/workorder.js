import request from '@/router/axios'

const baseUserUrl = '/api/vote/v1/workorder/'

// 新建工单
export function addWorkOrder (obj) {
  return request({
    url: baseUserUrl + 'addWorkOrder',
    method: 'post',
    data: obj
  })
}

// 获取工单列表
export function fetchList (query) {
  return request({
    url: baseUserUrl + 'workOrderList',
    method: 'get',
    params: query
  })
}

// 修改工单
export function updateObj (obj) {
  return request({
    url: baseUserUrl + 'updateWorkOrder',
    method: 'put',
    data: obj
  })
}
