// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import App from './App'
import router from './router'
import store from './store'
import './permission' // 权限控制
import 'element-ui/lib/theme-chalk/index.css'
import './icons' // icon
import Base64 from './utils/base64.js'

Vue.config.productionTip = false
Vue.prototype.$Base64 = Base64
Vue.use(ElementUI)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
