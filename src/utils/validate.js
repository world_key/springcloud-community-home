/**
 * 判断是否为空
 */
export function validatenull (val) {
  if (val instanceof Array) {
    if (val.length === 0) {
      return true
    }
  } else if (val instanceof Object) {
    if (JSON.stringify(val) === '{}') {
      return true
    }
  } else {
    if (val === 'null' || val === null || val === 'undefined' || val === undefined || val === '') {
      return true
    }
    return false
  }
  return false
}

// 验证密码   密码，以数字，大写字母，小写字母，特殊字符 至少两种或两种以上且最低6尾数
export function validaPass (str) {
  const reg = /^(?![A-Z]+$)(?![a-z]+$)(?!\d+$)(?![\W_]+$)\S{6,20}$/
  return reg.test(str)
}
