import { setStore, getStore } from '@/utils/store'

const party = {
  state: {
    party: getStore({
      name: 'party'
    }) || {}
  },
  actions: {
    SaveParty ({ commit }, party) {
      commit('SET_PARTY', party)
    }
  },
  mutations: {
    SET_PARTY: (state, party) => {
      state.party = party
      setStore({
        name: 'party',
        content: state.party
      })
    }
  }
}
export default party
