import { setStore, getStore } from '@/utils/store'

const community = {
  state: {
    community: getStore({
      name: 'community'
    }) || {}
  },
  actions: {
    SaveCommunity ({ commit }, community) {
      commit('SET_COMMUNITY', community)
    }
  },
  mutations: {
    SET_COMMUNITY: (state, community) => {
      state.community = community
      setStore({
        name: 'community',
        content: state.community
      })
    }
  }
}
export default community
