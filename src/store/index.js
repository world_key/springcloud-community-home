import Vue from 'vue'
import Vuex from 'vuex'

// 用户模块
import user from './modules/user'

// getters
import getters from './getters'

import sysConfig from './modules/sysConfig'

import community from './modules/community'

import party from './modules/party'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
    sysConfig,
    community,
    party
  },
  getters
})

export default store
