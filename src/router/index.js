import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const constantRouterMap = [
  {
    path: '/',
    name: 'Index',
    component: () => import('@/views/Index'),
    redirect: '/login',
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import('@/views/login/login'),
        meta: {
          title: '登录'
        }
      },
      {
        path: '/home',
        name: 'home',
        component: () => import('@/views/home/home'),
        meta: {
          title: '首页'
        }
      },
      {
        path: '/vote',
        name: 'vote',
        component: () => import('@/views/vote/vote'),
        meta: {
          title: '投票'
        }
      },
      {
        path: '/detail',
        name: 'detail',
        component: () => import('@/views/vote/detail'),
        meta: {
          title: '投票详情'
        }
      },
      {
        path: '/workorder',
        name: 'workorder',
        component: () => import('@/views/workorder/workorder'),
        meta: {
          title: '工单'
        }
      },
      {
        path: '/us',
        name: 'us',
        component: () => import('@/views/us/us'),
        meta: {
          title: '关于'
        }
      },
      {
        path: '/netuser',
        name: 'netuser',
        component: () => import('@/views/account/netuser'),
        meta: {
          title: '账户管理'
        }
      },
      {
        path: '/actAndPwd',
        name: 'actAndPwd',
        component: () => import('@/views/account/actAndPwd')
      },
      {
        path: '/message',
        name: 'message',
        component: () => import('@/views/account/message')
      },
      {
        path: '/personal',
        name: 'personal',
        component: () => import('@/views/account/personal'),
        meta: {
          title: '个人资料'
        }
      },
      {
        path: '/community',
        name: 'community',
        component: () => import('@/views/tabControl/community')
      },
      {
        path: '/publishArticle',
        name: 'publishArticle',
        component: () => import('@/views/article/publishArticle'),
        meta: {
          title: '写文章'
        }
      },
      {
        path: '/myArticle',
        name: 'myArticle',
        component: () => import('@/views/article/myArticle'),
        meta: {
          title: '我的文章'
        }
      },
      {
        path: '/releaseDecision',
        name: 'releaseDecision',
        component: () => import('@/views/article/releaseDecision'),
        meta: {
          title: '发布决策'
        }
      },
      {
        path: '/myDecision',
        name: 'myDecision',
        component: () => import('@/views/article/myDecision'),
        meta: {
          title: '我的决策'
        }
      },
      {
        path: '/decfordt',
        name: 'decfordt',
        component: () => import('@/views/vote/decfordt'),
        meta: {
          title: '小区决策'
        }
      }
    ]
  },
  {
    path: '*',
    redirect: '/login'
  }
]
export default new Router({
  routes: constantRouterMap
})
